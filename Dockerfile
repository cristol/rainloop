FROM alpine:3.21.3
LABEL authors="Cristol Bardou"

ARG WWW_PATH=/var/www/rainloop

RUN apk add --no-cache \
    shadow \
    tzdata \
    openssl \
    nginx \
    php \
    php-common \
    php-curl \
    php-xml \
    php-fpm \
    php-json \
    php-mysqli \
    php-iconv \
    php-dom \
    php-pdo_sqlite \
    wget \
    unzip && \
    rm -rf /usr/lib/perl* /usr/share/perl*

RUN wget http://www.rainloop.net/repository/webmail/rainloop-latest.zip && \
    unzip rainloop-latest.zip -d $WWW_PATH && \
    rm rainloop-latest.zip && \
    sed -i 's/user nginx/user nobody nobody/' /etc/nginx/nginx.conf && \
    sed -i 's/upload_max_filesize =.*/upload_max_filesize = 100M/g' /etc/php*/php.ini && \
    sed -i 's/post_max_size =.*/post_max_size = 100M/g' /etc/php*/php.ini

WORKDIR /app

ADD rootfs/ .

EXPOSE 80 443

VOLUME [ "/var/www/rainloop/data" ]

ENTRYPOINT ["sh", "./entrypoint.sh"]

# Rainloop Dockerfile

[![pipeline status](https://gitlab.com/cristol/rainloop/badges/master/pipeline.svg)](https://gitlab.com/cristol/rainloop/-/commits/master) 

> Création d'une image Rainloop sur alpine avec Nginx et php8.1

> [Registry Docker Hub](https://hub.docker.com/r/cristol/rainloop)

## Deploiement depuis docker hub

### Docker run

```bash
docker run -d --name Rainloop \
  -e PUID=1000 \
  -e PGID=1000 \
  -e DOMAIN=YourDomain \
  -e TZ=Europe/Paris \
  -v ~/.Rainloop_Data:/var/www/rainloop/data \
  -v YourCertificateFile:/etc/nginx/cert/fullchain.pem:ro \
  -v YourCertificateKeyFile:/etc/nginx/cert/privkey.pem:ro \
  -p 8080:80 \
  -p 8443:443 \
  cristol/rainloop:latest
```

### Docker compose

```yml
version: "3.8"
services:
  rainloop:
    image: cristol/rainloop:latest
    container_name: Rainloop
    restart: always
    environment:
      - PUID=1000
      - PGID=1000
      - DOMAIN=YourDomain
      - TZ=Europe/Paris
    volumes:
      - ~/.Rainloop_Data:/var/www/rainloop/data
      - YourCertificateFile:/etc/nginx/cert/fullchain.pem:ro
      - YourCertificateKeyFile:/etc/nginx/cert/privkey.pem:ro
    ports:
      - 8080:80
      - 8443:443
```

## Deploiment depuis le Dockerfile

### Création de l'image

```bash
docker build -t cristol/rainloop:latest .
```

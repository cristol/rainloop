#!/bin/sh

source ./scripts.d/fonctions.sh

# Execution des scripts presents dans scripts.d
for script in $(ls ./scripts.d/[0-9]*.sh); do
  sh $script
done

# Démarrage de php-fpm
php-fpm83
if [ $? -ne 0 ]; then
  LOG ERROR "Lancement de php-fpm en erreur"
  exit 1
fi

LOG INFO "Démarrage d'Nginx"
# Démarrage d'Nginx sans détachement
nginx -g "daemon off;"
if [ $? -ne 0 ]; then
  LOG ERROR "Lancement d'Nginx en erreur"
  exit 1
fi

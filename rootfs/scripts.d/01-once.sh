#!/bin/sh

REPSCRIPT=$(cd $(dirname $0); pwd)
source $REPSCRIPT/fonctions.sh

# Set PUID, PGID et DOMAIN
PUID=${PUID:-65534}
PGID=${PGID:-65534}
DOMAIN=${DOMAIN:-_}
TZ=${TZ:-Europe/Paris}

echo "PUID = $PUID"
echo "PGID = $PGID"
echo "DOMAIN = $DOMAIN"
echo "TZ = $TZ"

# Modification du TZ
CONTINENT=$(echo $TZ | awk -F/ '{print $1}')
CAPITALE=$(echo $TZ | awk -F/ '{print $2}')
cp /usr/share/zoneinfo/$CONTINENT/$CAPITALE /etc/localtime
if [ $? -ne 0 ]; then
  LOG ERROR "Le timezone $TZ semble invalide"
  exit 1
fi
echo $TZ > /etc/timezone
echo "Date actuelle avec le timezone $TZ : $(date)"

# Modification du PUID et PGID pour nobody
usermod -o -u $PUID nobody > /dev/null 2>&1
groupmod -o -g $PGID nobody > /dev/null 2>&1

if [ $DOMAIN == "_" ]; then
  LOG INFO "Activation du Vhost en site par defaut"
  sed -i "s/DOMAIN/$DOMAIN/g" ./confs/domain.conf
  cp ./confs/domain.conf /etc/nginx/http.d/default.conf
else
  LOG INFO "Activation du Vhost pour $DOMAIN"
  sed -i "s/DOMAIN/$DOMAIN/g" ./confs/domain.conf
  cp ./confs/domain.conf /etc/nginx/http.d/$DOMAIN.conf
  cp ./confs/default.conf /etc/nginx/http.d/default.conf
fi

# Generation de certificat
mkdir -p /etc/nginx/cert
cd /etc/nginx/cert

# Creation d'un certificat autosigne si non present.
if [[ ! -f fullchain.pem ]] || [[ ! -f privkey.pem ]]; then
  LOG WARN "Generation du certificat auto signé en cours"
  openssl req -new -newkey rsa:4096 -x509 -sha256 -days 3650 -nodes -out fullchain.pem -keyout privkey.pem \
  -subj "/C=FR/ST=Paris/L=Paris/O=Cristol_Rainloop/OU=Cristol&CO/CN=$DOMAIN" > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    LOG ERROR "Generation du certificat auto signé impossible"
  else
    LOG INFO "Generation du certificat auto signé OK"
  fi
else
  LOG INFO "Certificat deja existant"
fi

# Ajout du provider Orange
mkdir -p /var/www/rainloop/data/_data_/_default_/domains/
if [ ! -f /var/www/rainloop/data/_data_/_default_/domains/orange.fr.ini ]; then
  LOG INFO "Ajout des configuration pour le provider orange.fr"
  echo 'imap_host = "imap.orange.fr"
  imap_port = 993
  imap_secure = "SSL"
  imap_short_login = Off
  sieve_use = Off
  sieve_allow_raw = Off
  sieve_host = ""
  sieve_port = 4190
  sieve_secure = "None"
  smtp_host = "smtp.orange.fr"
  smtp_port = 465
  smtp_secure = "SSL"
  smtp_short_login = Off
  smtp_auth = On
  smtp_php_mail = Off' > /var/www/rainloop/data/_data_/_default_/domains/orange.fr.ini
fi
if [ ! -f /var/www/rainloop/data/_data_/_default_/domains/wanadoo.fr.alias ]; then
  LOG INFO "Ajout de l'alias wanadoo.fr pour le provider orange.fr"
  echo 'orange.fr' > /var/www/rainloop/data/_data_/_default_/domains/wanadoo.fr.alias
fi

# Suppression de ce script
cd $OLDPWD
rm $0

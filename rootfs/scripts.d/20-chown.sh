#!/bin/bash

REPSCRIPT=$(cd $(dirname $0); pwd)
source $REPSCRIPT/fonctions.sh

LOG INFO "Analyse des droits en cours"
# Chown du www
chown -R nobody:nobody /var/www/rainloop
chown -R nobody:nobody /var/lib/nginx
